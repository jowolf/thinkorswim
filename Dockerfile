#FROM rtfpessoa/ubuntu-jdk8
#FROM docker.io/rtfpessoa/ubuntu-jdk8
#FROM docker.io/library/ubuntu-jdk8
FROM docker.io/azul/zulu-openjdk:11-latest
#FROM docker.io/computesoftware/zulu-openjdk-11

#WORKDIR /home/ubuntu
WORKDIR /root

# Requirements
RUN apt-get update && apt-get install -y \
	wget \
	gconf2 \
	libnotify4 \
	libnss3 \
	libappindicator1 \
	libxss1 \
	libasound2 \
	mc

# ToS
RUN TEMP_DIR="/tmp/thinkorswim"
RUN wget -O "${TEMP_DIR}/thinkorswim_installer.sh" 'https://mediaserver.thinkorswim.com/installer/InstFiles/thinkorswim_installer.sh'
RUN chmod +x "$TEMP_DIR/thinkorswim_installer.sh"
#RUN sh -c 'echo "1\ro\r1\r1\r1\r\ry\rn\ry\r" | ${TEMP_DIR}/thinkorswim_installer.sh'
#RUN sh -c 'echo "1\ro\r1\r1\r1\r\ry\ry\r" | ${TEMP_DIR}/thinkorswim_installer.sh'
RUN sh -c 'echo "1\ro\r1\r1\r1\r\ry\rn\r" | ${TEMP_DIR}/thinkorswim_installer.sh'
#RUN sh -c 'echo "y\ry\r" | /usr/local/thinkorswim/thinkorswim'
#RUN "/usr/local/thinkorswim/thinkorswim"
RUN "/root/thinkorswim/thinkorswim"
CMD "/bin/bash"
