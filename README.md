# ThinkOrSwim Image

This repository provides a way to run TD Ameritrade's ThinkOrSwim in a Podman container.

July 2023 JJW:
- Converted to Podman
- Updated for Zulu JDK 11 
- Added mc (midnight commander) in containerr, to aid exploring / debugging
- Updated this README


## Requirements

- Podman, Buildah
- X Server


## Build

This script builds your image.

```bash
./build.sh
```

## Run

This script brings up a container, allowing you to login to the application.

```bash
./run.sh
```

If you get an error related to DISPLAY, you may have to run:

```bash
xhost +local:$(id -un)
```

NOTES:

1) If it hangs at "Installing updates", check the java version, current docs as of mid-2023 say Zulu JDK 11, so this is the starting docker image

2) Also, if it just ends, try commenting out the last line in run.sh, and run it manually:

joe@isc1:~/thinkorswim$ ./run.sh 
root@a04220dae678:~#     <-- you are now inside the container
root@a04220dae678:~# ./thinkorswim/thinkorswim   <-- run it manually
WARNING: An illegal reflective access operation has occurred
WARNING: Illegal reflective access by com.devexperts.jnlp.JavaLibraryPathUpdater (file:/root/thinkorswim/suit/1974.0.41/tos-suit-1974.0.41.jar) to field java.lang.ClassLoader.usr_paths
WARNING: Please consider reporting this to the maintainers of com.devexperts.jnlp.JavaLibraryPathUpdater
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release

These warnings do not prevent operation...

JJW